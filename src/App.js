import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import ProductView from './components/ProductView';
import UserOrder from './pages/UserOrder';
import Dashboard from './pages/Dashboard';
import AdminOrder from './pages/AdminOrder';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import ErrorPage from './pages/ErrorPage';
import { UserProvider } from './UserContext';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { useState } from 'react';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
      {/* Initializes dynamic routing */}
      <Router>
      <AppNavbar/>
        <Routes>
          <Route path="/" element={<Home/>}/>
          <Route path="/products" element={<Products/>}/>
          <Route path="/products/:productId" element={<ProductView/>}/>
          <Route path="/orders" element={<UserOrder/>}/>
          <Route path="/dashboard" element={<Dashboard/>}/>
          <Route path="/admin-orders" element={<AdminOrder/>}/>
          <Route path="/login" element={<Login/>}/>
          <Route path="/logout" element={<Logout/>}/>
          <Route path="/register" element={<Register/>}/>
          <Route path="*" element={<ErrorPage/>}/>
        </Routes>
      </Router>
      </UserProvider>
    </>
  );
}

export default App;
