import { Button, Row, Col, Modal, Form } from "react-bootstrap";
import { useState } from "react";
import Swal from "sweetalert2";
// import { useNavigate } from "react-router-dom";
// import AdminProduct from "../components/AdminProduct";


export default function Admin(props){
    const [show, setShow] = useState(false);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');

    // const [products, setProducts] = useState([])

    // const navigate = useNavigate()
    
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    
    function addProduct(event){
        event.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/products/create-product`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(response => response.json())
        .then(result => {
            if(result !== false){
                setName('')
                setDescription('')
                setPrice('')

                handleClose()

                Swal.fire({
                    title: 'Success!',
                    icon: 'success',
                    text: 'Successfully added a new Product!'
                })
                
                props.onRefreshData()
            } else {
                Swal.fire({
                    title: 'Something went wrong!',
                    icon: 'error',
                    text: 'Please try again.'
                })
            }
        })
    }

    return(
        <Row>
            <Col>
                <h1>Admin Dashboard</h1>
                <Button variant="success" onClick={handleShow}>Add New Product</Button>

                <Form>
                    <Modal show={show} onHide={handleClose}>
                        <Modal.Header closeButton>
                        <Modal.Title>Add Product</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                                <Form.Group>
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Enter Product Name"
                                        value={name}
                                        onChange={event => setName(event.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label>Description</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Enter Product Description"
                                        value={description}
                                        onChange={event => setDescription(event.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group>
                                    <Form.Label>Price</Form.Label>
                                    <Form.Control
                                        type="number"
                                        placeholder="Enter Product Price"
                                        value={price}
                                        onChange={event => setPrice(event.target.value)}
                                        required
                                    />
                                </Form.Group>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>
                                Close
                            </Button>
                            <Button variant="success" onClick={event => addProduct(event)}>
                                Add
                            </Button>
                        </Modal.Footer>
                    </Modal>
                </Form>
            </Col>
        </Row>
    )
}