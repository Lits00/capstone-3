import { Button, Form, Modal } from "react-bootstrap"
import PropTypes from 'prop-types'
import { useState } from "react"
import Swal from "sweetalert2";

export default function AdminProduct(props){
    const [show, setShow] = useState(false);
    const {name, description, price, _id, isActive} = props.product

    const [productName, setProductName] = useState('');
    const [productDescription, setProductDescription] = useState('');
    const [productPrice, setProductPrice] = useState('');
    
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    function editProduct(event){
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/update`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: productName,
                description: productDescription,
                price: productPrice
            })
        })
        .then(response => response.json())
        .then(result => {
            if(result !== false){
                setProductName("")
                setProductDescription("")
                setProductPrice("")

                handleClose()
                
                Swal.fire({
                    title: 'Success!',
                    icon: 'success',
                    text: 'Product successfully updated!'
                })
                
                // props.onRefreshData()
            } else {
                Swal.fire({
                    title: 'Something went wrong!',
                    icon: 'error',
                    text: 'Please try again.'
                })
            }
        })
    }
    
    function Disable(){
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(result => {
            if(result !== false){
                Swal.fire({
                    title: 'Success!',
                    icon: 'success',
                    text: 'Product successfully updated!'
                })
                
                // props.onRefreshData()
            } else {
                Swal.fire({
                    title: 'Something went wrong!',
                    icon: 'error',
                    text: 'Please try again.'
                })
            }
        })
    }

    function Enable(){
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/retrieve`, {
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(result => {
            if(result !== false){
                Swal.fire({
                    title: 'Success!',
                    icon: 'success',
                    text: 'Product successfully updated!'
                })
                
                // props.onRefreshData()
            } else {
                Swal.fire({
                    title: 'Something went wrong!',
                    icon: 'error',
                    text: 'Please try again.'
                })
            }
        })
    }

    return(
        <>
            <tr>
                <td>{_id}</td>
                <td>{name}</td>
                <td>{description}</td>
                <td>₱{price}</td>
                <td>
                    {isActive ? "In Stock" : "Out of Stock"}
                </td>
                <td>
                    <Button onClick={handleShow}>Edit</Button>
                    {(isActive) ?
                    <Button className="ms-2" variant="danger" onClick={Disable}>Disable</Button>
                    :
                    <Button className="ms-2" variant="success" onClick={Enable}>Enable</Button>
                    }
                    <Form>
                        <Modal show={show} onHide={handleClose}>
                            <Modal.Header closeButton>
                            <Modal.Title>Edit Product</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                    <Form.Group>
                                        <Form.Label>Name</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder={name}
                                            value={productName}
                                            onChange={event => setProductName(event.target.value)}
                                            required
                                        />
                                    </Form.Group>

                                    <Form.Group>
                                        <Form.Label>Description</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder={description}
                                            value={productDescription}
                                            onChange={event => setProductDescription(event.target.value)}
                                            required
                                        />
                                    </Form.Group>

                                    <Form.Group>
                                        <Form.Label>Price</Form.Label>
                                        <Form.Control
                                            type="number"
                                            placeholder={price}
                                            value={productPrice}
                                            onChange={event => setProductPrice(event.target.value)}
                                            required
                                        />
                                    </Form.Group>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="secondary" onClick={handleClose}>
                                    Close
                                </Button>
                                <Button variant="primary" onClick={event => editProduct(event)}>
                                    Update
                                </Button>
                            </Modal.Footer>
                        </Modal>
                    </Form>
                </td>
            </tr>
        </>
    )
}

AdminProduct.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}