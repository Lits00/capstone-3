import "../App.css";
import { Navbar, Nav, Container } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
// import { useContext } from "react";
// import UserContext from "../UserContext";
import logo from "../images/logo.png"


export default function AppNavbar(){

    // const {user} = useContext(UserContext)

    return(
        (localStorage.getItem('token') !== null) ?
            <Navbar className="color1" expand="lg" variant="dark">
                <Container>
                    <Navbar.Brand as={Link} to="/"><img className="logo" src={logo} alt="RN-Logo"></img></Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav" className="nav-route">
                        <Nav className="ml-auto">
                            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                            { (localStorage.getItem('admin')) && (
                                <>
                                    <Nav.Link as={NavLink} to="/dashboard">Dashboard</Nav.Link>
                                    <Nav.Link as={NavLink} to="/admin-orders">Orders</Nav.Link>
                                </>
                            )}
                            {  (localStorage.getItem('user')) && (
                                <>
                                    <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
                                    <Nav.Link as={NavLink} to="/orders">Orders</Nav.Link>
                                </>
                            )}
                            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        :
            <Navbar className="color1" expand="lg" variant="dark">
                <Container>
                    <Navbar.Brand as={Link} to="/"><img className="logo" src={logo} alt="RN-Logo"></img></Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav" className="nav-route">
                        <Nav className="ml-auto">
                            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                            <Nav.Link as={NavLink} to="/products">Products</Nav.Link>                         
                            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>       
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
    )
}