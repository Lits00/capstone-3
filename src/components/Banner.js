import { Button, Row, Col, Container} from 'react-bootstrap'
import { NavLink } from 'react-router-dom'

export default function Banner(){
    return(
        <Container>
            <Row>
                <Col className='p-5'>
                    <h1>RN Bake N' Brew</h1>
                    <p>Coffee, Cakes & Pastries!</p>
                    {(localStorage.getItem('admin')) ?
                    <Button as={NavLink} to="/products" variant="primary" hidden>Buy now!</Button>
                    :
                    <Button as={NavLink} to="/products" variant="primary">Buy now!</Button>
                    }
                </Col>
            </Row>
        </Container>
    )
}