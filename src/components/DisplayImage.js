import { Row, Col, Container} from 'react-bootstrap'
import cake from '../images/cake.jpg'
import pastry from '../images/pastry.jpg'
import coffee from '../images/coffee.jpg'
import "../App.css"


export default function DisplayImage(){
    return(
        <Container className='p-3'>
            <Row className='justify-content-md-center'>
                <Col xs lg="3">
                    <img src={coffee} alt="coffee"></img>
                </Col>
                <Col xs lg="3">
                    <img src={cake} alt="cake"></img>
                </Col>
                <Col xs lg="3">
                    <img src={pastry} alt="pastry"></img>
                </Col>
            </Row>
        </Container>
    )
}