import { Table } from "react-bootstrap"
import { useEffect, useState } from "react";
// import PropTypes from 'prop-types'
import moment from 'moment'

export default function GetOrders(){
    // const {userId, _id, purchasedOn, products, quantity, name, price, totalAmount} = order
    const [orders, setOrders] = useState([]);

    useEffect(() => {

        fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(result => {
            setOrders(
                result.map(order =>{
                    let prodData = "";
                    order.products.forEach(data => {
                        prodData += `${data.quantity} - ${data.name}`
                    });
                    return (
                        <tr>
                            <td>{order.userId}</td>
                            <td>{order._id}</td>
                            <td>{moment(order.purchasedOn).format("LL")}</td>
                            <td>
                                {prodData}
                            </td>
                            {/* <td>{order.products.quantity}</td> */}
                            <td>₱ {order.totalAmount}</td>
                        </tr>
                    )
                })
            )   
        })
    }, [])

    return(
        <>
            <Table stripped bordered hover variant="dark">
                <thead>
                    <tr>
                        <th>UserID</th>
                        <th>OrderID</th>
                        <th>Date of Purchase</th>
                        <th>Product/s</th>
                        <th>Total Amout</th>
                    </tr>
                </thead>
                <tbody>
                   {orders}
                </tbody>
            </Table>
        </>
    )
}

// GetOrders.propTypes = {
//     order: PropTypes.shape({
//         userId: PropTypes.string.isRequired,
//         purchasedOn: PropTypes.string.isRequired,
//         products: PropTypes.array.isRequired,
//         totalAmount: PropTypes.number.isRequired
//     })
// }