import { Container } from 'react-bootstrap'
import Spinner from 'react-bootstrap/Spinner'

export default function Loading(){
    return(
        <Container className='center'>
            <Spinner animation="border" variant="primary" />
        </Container>
    )
}