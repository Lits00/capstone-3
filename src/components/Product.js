import PropTypes  from "prop-types"
import { Card, Col, Row } from "react-bootstrap"
import { Link } from "react-router-dom"

export default function Product({product}){
    const {name, description, price, _id} = product

    return(
        <Row>
            <Col>
                <Card>
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price: ₱{price}</Card.Subtitle>
                        <Link to={`/products/${_id}`}>Details</Link>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}

Product.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}