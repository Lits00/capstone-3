import { useContext, useEffect, useState } from "react";
import { useParams, Link, useNavigate } from "react-router-dom";
import { Container, Row, Col, Card, Button, Form } from "react-bootstrap";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import "../App.css"


export default function ProductView(){
    const {productId} = useParams();

    const {user} = useContext(UserContext);

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [isAvailable , setIsAvailable] = useState("");
    const [quantity, setQuantity] = useState(1);

    const navigate = useNavigate()
    function Back(){
        navigate("/products")
    }

    const order = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/order-product`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                userId: user.id,
                productId: productId,
                name: name,
                quantity: quantity
            })
        })
        .then(response => response.json())
        .then(result => {
            if(result){
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Order successful!"
                })
            } else {
                Swal.fire({
                    title: "Something went wrong!",
                    icon: "error",
                    text: "Please try again :("
                })
            }
        })
    }

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
        .then(response => response.json())
        .then(result => {
            setName(result.name)
            setDescription(result.description)
            setPrice(result.price)
            setIsAvailable(result.isActive)
        })
    }, [productId])

    return(
        <Container className="mt-5">
        <Row>
            <Col lg={{ span: 6, offset: 3 }}>
                <Card>
                    <Card.Body className="text-center">
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price: ₱{price}</Card.Subtitle>
                        <Form className="mt-3">
                            <Card.Subtitle>Quantity:</Card.Subtitle>
                            <Form.Control className="quality center mt-2"
                                type="number"
                                min={1}
                                placeholder="1"
                                value={quantity}
                                onChange={event => setQuantity(event.target.value)}
                                required
                            />
                        </Form>
                       <Button className="me-2 mt-2" onClick={() => Back()}>Go Back</Button>
                        {   user.id !== null ?
                                (isAvailable) ? 
                                <Button className="mt-2" variant="primary" onClick={() => order(productId)}>Order</Button>
                                :
                                <Button className="mt-2" variant="secondary" disabled>Unavailable</Button>
                            :
                                <Link className="btn btn-danger btn-block mt-2" to="/login">Log in to Order</Link>
                        }
                    </Card.Body>		
                </Card>
            </Col>
        </Row>
    </Container>
    )
}