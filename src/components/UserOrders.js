import { Table } from "react-bootstrap"
// import { useEffect, useState } from "react";
import PropTypes from 'prop-types'
import moment from 'moment'

export default function UserOrders({order}){
    // const {_id, purchasedOn, products, totalAmount} = order
    // const [orders, setOrders] = useState([]);

    // useEffect(() => {

    //     fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/orders`, {
    //         headers: {
    //             "Content-Type": "application/json",
    //             Authorization: `Bearer ${localStorage.getItem('token')}`
    //         }
    //     })
    //     .then(response => response.json())
    //     .then(result => {
    //         setOrders(
    //             result.map(order =>{
    //                 return (
    //                     <tr>
    //                         <td>{order.userId}</td>
    //                         <td>{order._id}</td>
    //                         <td>{order.purchasedOn}</td>
    //                         <td></td>
    //                         {/* <td>{order.products[i]}</td> */}
    //                         <td>₱ {order.totalAmount}</td>
    //                     </tr>
    //                 )
    //             })
    //         )   
    //     })
    // }, [])

    return(
        <>
            <tr>
                <th>{order._id}</th>
                <th>{moment(order.purchasedOn).format("LL")}</th>
                <th>
                    {   
                        order.products.map(data => {
                            return(`${data.quantity} - ${data.name}`) 
                        })
                    }
                </th>
                {/* <th>{products.name}</th> */}
                <th>{order.totalAmount}</th>
            </tr>
        </>
    )
}

UserOrders.propTypes = {
    order: PropTypes.shape({
        _id: PropTypes.string.isRequired,
        purchasedOn: PropTypes.string.isRequired,
        products: PropTypes.array.isRequired,
        totalAmount: PropTypes.number.isRequired
    })
}