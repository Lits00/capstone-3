// import { useState } from "react";
import { Container } from "react-bootstrap";
import GetOrders from "../components/GetOrders";
// import Loading from "../components/Loading";

export default function AdminOrder(){
   
    // const [isLoading, setIsLoading] = useState(false)

   

    return(
        // (isLoading) ?
        //     <Loading/>
        // :
            <Container className="text-center mt-4">
                <h1>User Orders</h1>
                <GetOrders/>
            </Container>
    )
}