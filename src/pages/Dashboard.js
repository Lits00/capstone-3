import "../App.css"
import Admin from "../components/Admin";
import AdminProduct from "../components/AdminProduct";
import Loading from "../components/Loading";
import { useEffect, useState } from "react";
import { Container, Table } from "react-bootstrap";
// import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";

export default function Dashboard(){
    // const {user} = useContext(UserContext)
    const [products, setProducts] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    const [refetchData, setRefetchData] = useState(false)

    // function refreshPage(){
    //     setRefetchData(true)
    //     fetch(`${process.env.REACT_APP_API_URL}/products/product-list`)
    //     .then(response => response.json())
    //     .then(result => {
    //         setProducts(
    //             result.map(product => {
    //                 return(
    //                     <AdminProduct key={product._id} product={product}/>
    //                 )
    //             })
    //         )
    //         setRefetchData(false)
    //     })
    // }

    // const refresh = ()=>{
    //     window.location.reload();
    // }

    useEffect((isLoading) => {
        setIsLoading(true)

        fetch(`${process.env.REACT_APP_API_URL}/products/product-list`)
        .then(response => response.json())
        .then(result => {
            setProducts(
                result.map(product => {
                    return(
                        <AdminProduct key={product._id} product={product} />
                        
                    )
                })
            )

            setIsLoading(false)
        })
    }, [])

    useEffect(() => {
        setRefetchData(true)

        fetch(`${process.env.REACT_APP_API_URL}/products/product-list`)
        .then(response => response.json())
        .then(result => {
            setProducts(
                result.map(product => {
                    return(
                        <AdminProduct key={product._id} product={product}/>
                    )
                })
            )
            setRefetchData(false)
        })
    }, [refetchData])

    return(
        (isLoading) ?
            <Loading/>
        :
        (localStorage.getItem('admin')) ?
            <Container>
                <div className="center mt-4">
                    <Admin />
                    <Table stripped bordered hover variant="dark" className="mt-3">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Price</th>
                                <th>Availability</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        {products}
                        </tbody>
                    </Table>
                </div>
            </Container>
        :
            <Navigate to="/"/>
    )
}