import { Container } from "react-bootstrap"
import { Link } from "react-router-dom"
import "../App.css"

export default function ErrorPage(){
    return (   
        <Container className="center mt-5 pt-5">
            <h1>Oops!</h1>
            <p>That page was not found. :(</p>
            <Link to="/">Go back to Home page.</Link>
        </Container>
    )
}