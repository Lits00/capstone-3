import Banner from "../components/Banner";
import DisplayImage from "../components/DisplayImage"
import "../App.css"

export default function Home() {

    return(
        <>
            <div className="center">
                <Banner/>
                <DisplayImage/>
            </div>
        </>
    )
}