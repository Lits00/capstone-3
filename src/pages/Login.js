import "../App.css"
import { useContext, useState, useEffect } from "react";
import UserContext from "../UserContext";
import { useNavigate, Navigate } from "react-router-dom";
import { Form, Button, Container, Modal } from "react-bootstrap";
import Swal from "sweetalert2";


export default function Login(){
    const {setUser} = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const navigate = useNavigate();

    const [isActive, setIsActive] = useState(false)

    const retrieveUser = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(result => {
            if(result.isAdmin === true){
                localStorage.setItem('admin', result.isAdmin)
            } else {
                localStorage.setItem('user', true)
            }
            // Store the user details retrieved from the token into the global user state
            setUser({
                id: result._id,
                isAdmin: result.isAdmin
            })
        })
    }

    function authenticate(event){
        event.preventDefault()
        
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(response => response.json())
        .then(result => {
           if(typeof result.accessToken !== "undefined"){
                localStorage.setItem('token', result.accessToken)

                retrieveUser(result.accessToken)

                Swal.fire({
                    title: 'Login Successful!',
                    icon: 'success',
                    text: 'Enjoy Shopping!'
                })

                navigate('/')

            } else {
                Swal.fire({
                    title: 'Authentication Failed!',
                    icon: 'error',
                    text: 'Please try again.'
                })
            }
        })
    }

    useEffect(()=>{
        if(email !== '' && password !== ''){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    return(   
        (localStorage.getItem('token') !== null) ?
        <Navigate to="/"/>
        :
        <Container className="container-style">
            <Modal.Dialog className="box-style mt-5">
                <Modal.Header className="pb-2">
                    <Modal.Title>Login</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={event => authenticate(event)}>
                        <Form.Group controlId="userEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                                type="email" 
                                placeholder="Enter email"
                                value={email}
                                onChange={event => setEmail(event.target.value)} 
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Password"
                                value={password}
                                onChange={event => setPassword(event.target.value)} 
                                required
                            />

                        </Form.Group>
                        {   isActive ?
                            <Button className="mt-3" variant="success" type="submit" id="submitBtn">
                                Login
                            </Button>
                            :
                            <Button className="mt-3" variant="secondary" type="submit" id="submitBtn" disabled>
                                Login
                            </Button>
                        }
                    </Form>
                </Modal.Body>
            </Modal.Dialog>
        </Container>
    )
}