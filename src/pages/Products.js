import { useEffect, useState } from "react";
import Loading from "../components/Loading";
import Product from "../components/Product";
import "../App.css"
import { Container } from "react-bootstrap";


export default function Products(){
    const [products, setProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    useEffect((isLoading) => {
        setIsLoading(true);

        fetch(`${process.env.REACT_APP_API_URL}/products/product-list`)
        .then(response => response.json())
        .then(result => {
            setProducts(
                result.map(product => {
                    return(
                        <Product key={product._id} product={product}/>
                    )
                })
            )

            setIsLoading(false)
        })
    }, [])

    return(
        (isLoading) ?
            <Loading/>
        :
            <Container className="center">
                <h1 className="text-center mt-5">RN Bake N' Brew</h1>
                <div className="display">
                    {products}
                </div>
            </Container>
    )
}