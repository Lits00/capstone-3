import "../App.css"
import { useEffect, useState } from "react";
import { Form, Button, Container, Modal } from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from 'sweetalert2'

export default function Register(){
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [mobileNumber, setMobileNumber] = useState('');

    const navigate = useNavigate()

    const [isActive, setIsActive] = useState(false)

    function registerUser(event){
        event.preventDefault()
        console.log(`${process.env.REACT_APP_API_URL}/users/register`)
        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password1,
                mobileNo: mobileNumber
            })
        })
        .then(response => response.json())
        .then(result => {
            console.log(result)
            if(result !== false){
                // Clears out the input fields after submission
                setEmail('');
                setPassword1('');
                setPassword2('');
                setMobileNumber('');

                Swal.fire({
                    title: 'Registration successful!',
                    icon: 'success',
                    text: 'Welcome to RN Bake N\' Brew!'
                });

                navigate("/login");
            // } else if(result === {message: 'User already exist.'}){
            //     Swal.fire({
            //         title: 'Something went wrong!',
            //         icon: 'error',
            //         text: 'Email is already in use!'
            //     });
            } else {
                Swal.fire({
                    title: 'Something went wrong!',
                    icon: 'error',
                    text: 'Please try again!'
                });
            };
        });
    };

    useEffect(() =>{
        if((email !== '' && password1 !== '' && password2 !== '' && mobileNumber.length === 11) && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password1, password2, mobileNumber]);



    return(
        (localStorage.getItem('token') !== null) ?
            <Navigate to="/"/>
        :
        <Container>
            <Modal.Dialog className="box-style mt-5">
                <Modal.Header className="pb-2">
                    <Modal.Title>Registration</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <Form onSubmit={event => registerUser(event)}>
                    <Form.Group controlId="userEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email"
                            value={email}
                            onChange={event => setEmail(event.target.value)} 
                            required
                        />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group controlId="password1">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            placeholder="Password"
                            value={password1}
                            onChange={event => setPassword1(event.target.value)} 
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="password2">
                        <Form.Label>Verify Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            placeholder="Verify Password"
                            value={password2}
                            onChange={event => setPassword2(event.target.value)} 
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="mobileNumber">
                        <Form.Label>Mobile Number</Form.Label>
                        <Form.Control 
                            type="text"
                            placeholder="Enter mobile number"
                            value={mobileNumber}
                            onChange={event => setMobileNumber(event.target.value)} 
                            required
                        />
                    </Form.Group>

                    {   isActive ?
                        <Button className="mt-3" variant="success" type="submit" id="submitBtn">
                            Submit
                        </Button>
                        :
                        <Button className="mt-3" variant="secondary" type="submit" id="submitBtn" disabled>
                            Submit
                        </Button>
                    }
                </Form>
                </Modal.Body>
            </Modal.Dialog>
        </Container>
    );
};