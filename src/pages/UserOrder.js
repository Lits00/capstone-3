import { useContext, useState, useEffect } from "react";
import { Container, Table } from "react-bootstrap";
import UserOrders from "../components/UserOrders";
import UserContext from "../UserContext";

export default function UserOrder(){
    const {user} = useContext(UserContext)
    const [orders, setOrders] = useState([]);

    useEffect(() => {

        fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/orders`, {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(result => {
            setOrders(
                result.map(order =>{
                    return (
                        <UserOrders key={order._id} order={order}/>
                    )
                })
            )   
        })
    }, [])

    return(
            <Container className="text-center mt-4">
                <h1>Your Orders</h1>
                <Table stripped bordered hover variant="dark">
                    <thead>
                        <tr>
                            <th>OrderID</th>
                            <th>Date of Purchase</th>
                            <th>Product/s</th>
                            <th>Total Amout</th>
                        </tr>
                    </thead>
                    <tbody>
                        {orders}
                    </tbody>
                </Table>
            </Container>
    )
}